.PGNH
.\"
.\" Document registers - *=you set. x=calculated:
.\"
.\" page.type        A4, LETTER or TERMINAL             (*)
.\" page.leftMargin  left margin                        (*)
.\" page.rightMargin right margin                       (*)
.\" page.width       page width                         (x)
.\" page.length      page length                        (x)
.\" page.lineLength  line length                        (x)
.\" page.topMargin   top of page to text                (*)
.\" page.headMargin  top of page to header              (*)
.\" page.footMargin  bottom of footer to bottom of page (*)
.\" page.botMargin   bottom of text to bottom of page   (*)
.\" cols             number of columns                  (*)
.\" col.spacing      column spacing                     (*)
.\" col.width        column width                       (x)
.\" vl.textIndent    text indent for .VL lists          (*)
.\" vl.markIndent    mark indent for .VL lists          (*)
.\"
.\"-------------------------------------------------------------------------
.\"         USER PARAMETERS
.\" Also check the filename in the .so statement at the end.
.if n \{\
.tm Formatting for nroff
.ds page.type TERMINAL\" A4, LETTER or TERMINAL
.nr page.leftMargin 0
.nr page.rightMargin 3n
.nr page.topMargin 2v
.nr page.headMargin 0v
.nr page.footMargin 0
.nr page.botMargin 0
.nr col.spacing 1n
.nr cols 1
.nr vl.textIndent 5n
.nr vl.markIndent 0
.\}
.if t \{\
.\" the page length/foot/hdr settings are hit and miss. This seems to
 \" (almost) work:
.tm Formatting for troff
.ds page.type A4\" A4, LETTER or TERMINAL
.nr page.leftMargin 2n
.nr page.rightMargin 0
.nr page.topMargin 0v
.nr page.headMargin 0v
.nr page.footMargin 0v
.nr page.botMargin 3v
.nr col.spacing 1n
.nr cols 3
.nr vl.textIndent 6n
.nr vl.markIndent 0
.\}
.\"
.\"-------------------------------------------------------------------------
.\"         CALCULATIONS:
.\"
.if '\*[page.type]'A4' \{\
.tm Formatting for A4 paper (210mm x 297mm)
.nr page.length 29.7c
.nr page.width 21c
.\}
.if '\*[page.type]'LETTER' \{\
.tm Formatting for US Letter paper (8i x 11i)
.nr page.length 11i
.nr page.width 8.5i
.\}
.if '\*[page.type]'TERMINAL' \{\
.tm Formatting for TERMINAL
.nr page.length 60v
.nr page.width 75n
.\}
.\"
.\" Let command line options override the above:
.if r L .nr page.length \nL
.if r W .nr page.width \nW
.if r O .nr page.leftMargin \nO
.\"
.nr page.lineLength \n[page.width]-\n[page.rightMargin]-\n[page.leftMargin]
.nr page.length \n[page.length]u-\n[page.topMargin]u-\n[page.headMargin]u-\n[page.footMargin]u-\n[page.botMargin]u
.\" should be this: .nr col.width \n[page.lineLength]-((\n[col.spacing]+1n)*2)
.\" but this gives a better right hand margin for some reason. Sigh!
.nr col.width \n[page.lineLength]-((\n[col.spacing]+1n)*1)
.nr col.width \n[col.width]/\n[cols]
.\" 'man groff_mm' suggests that .PGFORM is broken but '.nr O ...'
 \" does not appear to set page offset
.PGFORM \n[page.lineLength]u \n[page.length]u \n[page.leftMargin]u 1
.\"nr L \n[page.length]u
.\"nr W \n[page.width]u
.\"nr O \n[page.leftMargin]u
.\"VM -T \n[page.topMargin]u+\n[page.headMargin]u \n[page.footMargin]u+\n[page.botMargin]u
.na\"       no right adjustment
.\"
.\"-------------------------------------------------------------------------
.\"     MACROS
.\" Print title & initialise boxing:
.de HD
.P
.\" Draw horizontal line
.if !\\$2 \l'\n[col.width]u'
.ne .5i
.ce 1
\\fI\\$1\\fR
..
.\" Subscript:
.de Sb
\d\s-2\\$1\s+2\u
..
.\" Provide me's date strings:
.if \n(mo=1 .ds mo January
.if \n(mo=2 .ds mo February
.if \n(mo=3 .ds mo March
.if \n(mo=4 .ds mo April
.if \n(mo=5 .ds mo May
.if \n(mo=6 .ds mo June
.if \n(mo=7 .ds mo July
.if \n(mo=8 .ds mo August
.if \n(mo=9 .ds mo September
.if \n(mo=10 .ds mo October
.if \n(mo=11 .ds mo November
.if \n(mo=12 .ds mo December
.if \n(dw=1 .ds dw Sunday
.if \n(dw=2 .ds dw Monday
.if \n(dw=3 .ds dw Tuesday
.if \n(dw=4 .ds dw Wednesday
.if \n(dw=5 .ds dw Thursday
.if \n(dw=6 .ds dw Friday
.if \n(dw=7 .ds dw Saturday
.ds td \*(mo \n(dy, 19\n(yr
.\"
.\" Provide line break for long marks (in .VL lists):
.de Li
.ne 2v
.LI "\\$1" "\\$2"
.if \\w"\\$1">\\n[vl.textIndent]-\\n[vl.markIndent] .br
..
.de DSL \" Display Start
.DS
..
.de DSCB \" Display Start Centered as a Block
.DS CB
..
.de DFL \" Floating Display
.DF
..
.ds eqn.string .DS ... .DE display
.de Code \" equiv to <CODE>
.P
.S -1
.nf
.na
.nh
\fC
..
.de CodeEnd
.fi
.ad
.hy
\fP
.S +1
.P
..
.\"-------------------------------------------------------------------------
.\"     HEADERS & COLUMNS
.ds month \n(mo
.if \n(mo<10 .ds month 0\n(mo
.ds dy \n(dy
.if \n(dy<10 .ds dy 0\n(dy
.ds hours \n[hours]
.if \n[hours]<10 .ds hours 0\n[hours]
.ds minutes \n[minutes]
.if \n[minutes]<10 .ds minutes 0\n[minutes]
.ds version \n[year]\*[month]\*(dy:\*[hours]\*[minutes]
.if t \{\
.\" Replacement for top of page processing - draw vertical lines:
.de TP
.if \\n[%]>1 .sp
.mk z
.nr l \\n[page.length]
.nr w \\n[col.width]
.nr b \\nw*2+\\n[col.spacing]+.5n
\v'-1v'\h'\\nwu+.5n'\L'\\nlu'\h'\\nwu+\\n[col.spacing]u'\L'-\\nlu'\h'-\\nbu'
.sp |\\nzu
..
.\}
.PF "'\fBPage %\fP'\fB-mm Quick Reference\fP'\fB\*[version]\fP'"
.if \n[cols]>1 .MC \n[col.width]u \n[col.spacing]u
.\"
.\"-------------------------------------------------------------------------
.\"     BODY OF TEXT
.\" Draw vertical lines on first page:
.if t .TP
.ce 100
\fBQuick Reference Guide\fP
.br
for \fBmm\fP and \fBgtroff\fP (ver. 3)
.br
.ce 0
.SP 1
Copyright \(co Bob Hepple 1985, 1994, 1995, 2018 version \*[version]
.P
All commands are \fBmm\fP unless marked \fBt\fP.
See 'man groff_mm' and 'man 7 groff' for details.
.P
.\"
.\"-------------------------------------------------------------------------
.HD "Page Control"
.P
Headers are controlled by \enN:
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.LI 0:
print on all pages
.LI 1:
page 1 header moved to footer
.LI 2:
no page 1 header
.LI 3:
page numbers as footer (\fIsection no\fP-\fIpage no\fP) and level 1
headings start on a new page
.LI 4:
no header on page 1 and on other pages only print explicit .PH header
.LI 5:
same as 3 and .FG numbering
enabled.
.LE
Note that \e sequences in header strings need doubling.
.P
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.PGFORM\fP [L [P [O [1]]]]"
Sets linelength, pagelength and/or pageoffset.
1 suppresses line break.
.Li "\fB.VM\fP [-T] [top [bottom]]"
Increase (use -T to set) vertical margin [7v 5v]
\fB.VM\fP alone resets.
.Li "\fB.SK\fP [pages]"
skip pages
.Li "\fB.ne\fP N"
need vertical space (conditional page feed) \fBt\fP
.Li "\fB.pn\fP expr\ N"
set next page number \fBt\fP
.Li "\fB.OH\fP/\fB.EH\fP/\fB.PH\fP \(lq'L'C'R'\(rq"
odd/even/both page header.
.Li "\fB.OF\fP/\fB.EF\fP/\fB.PF\fP \(lq'L'C'R'\(rq".
odd/even/both page footer.
.Li "\fB.TP, .PX\fP"
User defined macros instead of header (.TP) or as well as header (.PX)
.Li "\fB.PGNH\fP"
suppress header on next page
.Li "\fB.OP\fP"
skip to next odd page
.Li "\fB.2C\fP"
2 column mode
.Li "\fB.1C\fP [1]"
1 column mode; 1=no page feed
.Li "\fB.MC\fP width [spacing]"
Multi-column mode - groff calculates number of cols
.Li "\fB.NCOL\fP"
New column
.Li "\fB.WC\fP \fIargs\fP"
Footnote/display style with \fB.2C\fP [-WF -FF -WD FB]
.LE
.\"
.\"-------------------------------------------------------------------------
.HD "Paragraph Control"
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.P\fP [type]"
New paragraph.
0=left justified.
1=indented.
2=indented except after .H, .DE or .LE
.Li "\fB.H\fP level [heading-text [heading-suffix]]
Numbered heading.
.Li "\fB.HU\fP 'title'"
Unnumbered heading.
.Li ".nr \fBHb\fP N"
If level \(<= N [2] do .br before text
.Li ".nr \fBHs\fP {0-7}"
\(12 space after heading level \(<= N [2].
.Li ".ds \fBHP\fP 0 0 0 0 0 0 0"
Header point size (0=10pt)
.Li ".ds \fBHF\fP 2 2 2 2 2 2 2"
Fontlist for headings
.Li ".nr \fBHi\fP {0-2}"
Post-heading indent [1]. 0=text left justified. 1=use current
paragraph style. 2=Indent text to first word of heading.
.Li ".nr \fBHc\fP N"
Centre headings at level 1 to N [0].
.Li ".nr \fBEj\fP N"
Level \(<= N start on new page.
.Li ".nr \fBHt\fP {0-1}"
Suppress subsection number concatenation (4 instead of 3.2.4) [0]
.Li "\fB.HM\fP \fIlevel-1-style level-2-style ...\fP"
Heading numbering style with \fIlevel-n-style\fP from:
.LE
.TS
allbox,center,tab(;);
li lb
l l.
level-i;heading number
_
1;1, 2, 3, ...
001;001, 002, 003, ...
a;a, b, ... z, aa, bb, ...
A;A, B, ... Z, AA, BB, ...
i;i, ii, iii, ...
I;I, II, III, ...
.TE
.P
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.FG\fP [\fIcaption\fP [\fImodifier\fP [\fIcontrol\fP]]]"
Label a figure. \fIcontrol\fP [0] 0:\fImodifier\fP is a prefix,
1:\fImodifier\fP is a suffix, 2:\fImodifier\fP replaces number
.Li "\fB.TB\fP [\fIcaption\fP [\fImodifier\fP [\fIcontrol\fP]]]"
Label a table (params as .FG).
.Li "\fB.EC\fP [\fIcaption\fP [\fImodifier\fP [\fIcontrol\fP]]]"
Label an equation (params as .FG).
.Li "\fB.TC\fP \fIl1 n l2 leader-char a1 ... a3\fP"
Produce table of contents [1 1 2 0].
.LE
.\"
.\"-------------------------------------------------------------------------
.HD "Lists"
In general, the optional parameter [1] suppresses the space after the item.
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.LB\fP text-indent mark-indent pad type"
.Li "[mark [LI-space [LB-space]]]"
List begin macro (called by .VL, .AL etc)
.Li "\fB.BL\fP [text-indent [1]]"
Start bullet list
.Li "\fB.LI\fP [mark [1]]"
List item
.Li "\fB.ML\fP mark [text-indent]"
Marked list start
.Li "\fB.DL\fP [text-indent [1]]
Dashed list.
.Li "\fB.VL\fP text-indent [mark-indent [1]]"
Variable-item list start
.Li "\fB.AL\fP [type [text-indent [1]]]]"
Start autoincrement list
.Li "\fB.LE\fP"
list end
.LE
.\"
.\"-------------------------------------------------------------------------
.HD "Blocks & displays, footnotes"
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.DF\fP [format [fill [rindent]]]"
Begin floating display
.Li "\fB.DS\fP [format [fill [rindent]]]"
Begin static display [L N 0]. Format: L=left; C=centred; I=indent by \en(Si;
CB=centre as block. Fill: N=no fill; F=fill.
.Li "\fB.DE\fP"
End display
.Li "\fB.FD\fP [arg [1]]"
footnote default format
.Li "\fB.FS\fP [mark] ... \fB.FE\fP"
footnote
.Li "\fB\e*F\fP"
Insert automatic footnote number
.LE
.\"
.\"-------------------------------------------------------------------------
.HD "Line Control"
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.br\fP"
start new line \fBt\fP
.Li "\fB.fi\fP"
fill on \fBt\fP
.Li "\fB.nf\fP"
fill off \fBt\fP
.Li "\fB.na\fP"
right justification off \fBt\fP
.Li "\fB.ad\fP b"
right justification on \fBt\fP
.Li "\fB.ad\fP l"
flush left, ragged right \fBt\fP
.Li "\fB.ad\fP r"
flush right, ragged left \fBt\fP
.Li "\fB.ad\fP c"
centred \fBt\fP
.Li "\fB.ad\fP"
previous adjustment mode \fBt\fP
.Li "\fB.ce\fP n"
centre 'n' lines (n=0 stops centering) [1] \fBt\fP
.Li \fB.\e"\fP
comment at start of line \fBt\fP
.Li \fB\e"\fP
comment in the line!! \fBt\fP
.Li "\fB.hy\fP"
hyphenation on \fBt\fP
.Li "\fB.nh\fP"
hyphenation off \fBt\fP
.Li "\fB.ls\fP 2"
double space \fBt\fP
.Li "\fB.SP\fP [lines]"
vertical space
.LE
.de Sp
.Li \fB\\$1\fP
.if t \f[\\$2]\\$3\fP
.if n \\$3
..
.\"
.\"-------------------------------------------------------------------------
.HD "Character Control"
.VL \n[vl.textIndent]u \n[vl.markIndent]u 1
.Li "\fB.S\fP [size [spacing]]"
font size. C=current, D=default, P=previous, +/-.
.Li "\fB.SM\fP string1 [string2 [string3]]"
small font
.Li "\fB.SM\fP string1 [string2 [string3]]"
small font
.P
NB the remainder are \fBtroff\fP
.P
.\"
.\"-------------------------------------------------------------------------
.\" Now pull in the gtroff stuff. Point the filename as appropriate:
.so troff_quick.mme
.\"
.\"-------------------------------------------------------------------------
.\"     EMACS STUFF:
.\" Local Variables:
.\" mode:nroff
.\" eval:(setq filename (substring buffer-file-name (string-match "[a-zA-Z0-9_.]+$" buffer-file-name)))
.\" eval:(setq basename (substring filename 0 (string-match "\\\\." filename)))
.\" eval:(setq compile-command (concat "make -k " basename ".ps"))
.\" End:
