# To use other fonts, use:
# .ft H         Helvetica (cmss)
# .ft HI        Helvetica Italic (cmssi)
# .ft HB        Helvetica Bold (cmssbx)
# .ft CW        Constant Width (cmtt)
# .b, .bi and .i are pseudo font changes and actually move to RIB

ROFF=groff
PIC=-p
EQN=-e
#FONT=-fH
#DEV=-Tdvi -P-d  # The -P-d suppresses 'special' draw commands - only
                # horiz & vertical lines can be drawn

TAB=-t
#OPT=-P-w150 	#... for fat lines
#OPT=-P-l 	#... for postscript landscape 
.SUFFIXES: .txt .mm .me .ps

all: me_quick.txt me_quick.ps mm_quick.ps mm_quick.txt

clean:
	rm -f *.ps *.txt *~ [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].log

me_quick.ps: me_quick.me troff_quick.mme
	soelim me_quick.me| $(ROFF) -Tps -me $(FONT) $(PIC) $(EQN) $(TAB) $(OPT) >$*.ps

me_quick.txt: me_quick.me troff_quick.mme
	soelim me_quick.me| $(ROFF) -Tascii -me $(FONT) $(PIC) $(EQN) $(TAB) $(OPT) >$*.txt

mm_quick.ps: mm_quick.mm troff_quick.mme
	soelim mm_quick.mm| $(ROFF) -Tps -mm $(FONT) $(PIC) $(EQN) $(TAB) $(OPT) >$*.ps

mm_quick.txt: mm_quick.mm troff_quick.mme
	soelim mm_quick.mm| $(ROFF) -Tascii -mm $(FONT) $(PIC) $(EQN) $(TAB) $(OPT) >$*.txt
